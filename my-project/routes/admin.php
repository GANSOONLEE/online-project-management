<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/dashboard', function () {
  return view('backend.dashboard');
})->name("dashboard");

Route::get('/service', function () {
  return view('backend.service', [
    'services' => \App\Models\Service::orderBy("type")->paginate(10)
  ]);
})->name("service.create");

Route::post('/service/post', function (Request $request) {

  \App\Models\Service::create([
    "name" => $request->name,
    "description" => $request->description,
    "price" => $request->price,
    "type" => $request->type,
    "url" => $request->url,
    "disabled" => 0,
  ]);

})->name("service.post");

Route::patch('/service/patch/{id}', function (Request $request, $id) {
  $service = \App\Models\Service::where('id', $id)
    ->first();

  $service->update([
    "name" => $request->name ?? $service->name,
    "description" => $request->description ?? $service->description,
    "price" => $request->price ?? $service->price,
    "type" => $request->type ?? $service->type,
    "url" => $request->url ?? $service->url,
    "disabled" => $request->disabled ?? $service->disabled,
  ]);
  return redirect()->back()->with("success", "成功更新！");
})->name("service.patch");

Route::delete('/service/delete/{id}', function (Request $request, $id) {

  $service = \App\Models\Service::where('id', $id)
    ->delete();

})->name("service.delete");