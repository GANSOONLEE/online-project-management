<?php

use App\Models\Person;
use App\Models\Service;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ServiceController;


Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('/about', function () {
    return view('about');
})->name('about');

Route::get('/service', function () {
    $services = Service::orderBy("type", "desc")->get();
    return view('service', compact('services'));
})->name('service');

Route::get('/service/{type}', [
    ServiceController::class, 'index'
])->name('type');

Route::get('/services', [
    ServiceController::class, 'search'
])->name("search");

Route::get('/contact', function () {
    return view('contact');
})->name('contact');

Route::get('/404', function() {
    return view('build');
})->name('build');
