<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;

Route::get('/auth/logout', function () {
  Auth::logout();
  return redirect()->back();
})->name('logout');

Route::get('/auth', function () {
  return view('auth.login');
})->name('login');

Route::post('/auth/post', [
  AuthController::class, 'valid'
])->name('auth.login.post');