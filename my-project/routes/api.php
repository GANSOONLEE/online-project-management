<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');

// v1 Api
Route::group(['prefix'=> 'v1'], function () {
    Route::apiResource('person', App\Http\Controllers\Api\v1\PersonController::class);
    Route::apiResource('service', App\Http\Controllers\Api\v1\ServiceController::class);
});

// v2 Api
Route::group(['prefix'=> 'v2'], function () {
    Route::apiResource('person', App\Http\Controllers\Api\v2\PersonController::class);
});