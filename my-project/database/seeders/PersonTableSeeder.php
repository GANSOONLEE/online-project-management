<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Person;

class PersonTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Person::factory()
        //     ->count(50)
        //     ->create();

        Person::create([
            "last_name" => "frankgan0402",
            "email" => "vincentgan0402@gmail.com",
            "password" => bcrypt("99C39AEF0E"),
        ]);
    }
}
