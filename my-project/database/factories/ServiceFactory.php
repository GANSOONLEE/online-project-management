<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Service>
 */
class ServiceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            "name" => fake()->name(),
            "description" => fake()->text(120),
            "disabled" => fake()->boolean(),
            "price" => fake()->numberBetween(120, 3000),
            "type" => fake()->randomElement(["website", "merchandise", "other"]),
        ];
    }
}
