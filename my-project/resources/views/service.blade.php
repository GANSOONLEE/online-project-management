
@inject('totalServices', \App\Models\Service::class)

@extends('layouts.app')

@section('title', '服務項目')

@section('content')

  <h1 class="page-title">服務項目</h1>

  <section class="flex flex-col gap-y-6 w-full px-[4vw] font-notoSansTc">

    <!-- 搜索框 -->
    <div class="sticky top-16">
      <form method="get" action="{{ route("search") }}">
        <i class="fa fa-search absolute pl-3 pr-2 py-1 border-r border-gray-400 y-center"></i>
        <input value="{{ old('query') ?? $query ?? "" }}" type="text" name="query" class="search-input" placeholder="請輸入你要搜索的服務..."/>
      </form>
    </div>

    <div class="grid grid-cols-[minmax(240px,1fr)_4fr] gap-6">

      <!-- 分類 -->
      <div class="p-4 rounded-md shadow-xl bg-white border border-gray-200 h-min sticky top-32">
        
        <h3 class="text-xl font-bold font-sans">分類 ({{ $totalServices::scopeType()->count() }})</h3>
        
        <ul class="mt-3 pl-8 list-disc font-sans">
          
          <li class="service-link">
            <a href="{{ route('type', ["type" => "all"]) }}">
              全部 ({{ $totalServices::scopeType()->count() }})
            </a>
          </li>

          <li class="service-link">
            <a href="{{ route('type', ["type" => "website"]) }}">
              網站開發 ({{ $totalServices::scopeType("website")->count() }})
            </a>
          </li>

          <li class="service-link">
            <a href="{{ route('type', ["type" => "merchandise"]) }}">
              周邊商品 ({{ $totalServices::scopeType("merchandise")->count() }})
            </a>
          </li>
          
            <li class="service-link">
            <a href="{{ route('type', ["type" => "other"]) }}">
              其他 ({{ $totalServices::scopeType("other")->count() }})
            </a>
          </li>
        </ul>

      </div>

      <div class="bg-white border border-gray-200 rounded-md p-4">

        <h3 class="text-xl text-center">項目</h3>

        <section class="service-section">

          <!-- Service -->
          
            @if (count($services->all()) > 0)
  
              @foreach($services->all() as $service)

                <div class="service-box">
                  <img src="{{ $service->url ? 'https://' . $service->url : 'https://img.freepik.com/free-vector/organic-flat-about-me-landing-page_23-2148881669.jpg' }}" alt="" class="w-full h-full object-cover max-h-[120px]">
                  <div class="p-4 px-5 flex flex-col">
                    <h5 class="text-lg">
                      @if (isset($query) && stripos($service->name, $query) !== false)
                        {!! str_ireplace($query, '<span style="background-color: yellow;">'.ucfirst($query).'</span>', $service->name) !!}
                      @else
                        {{ $service->name }}
                      @endif
                    </h5>
                    
                    <p class="text-sm text-gray-600 leading-4">
                      @if (isset($query) && stripos($service->description, $query) !== false)
                        {!! str_ireplace($query, '<span style="background-color: yellow;">'.$query.'</span>', $service->description) !!}
                      @else
                        {{ $service->description }}
                      @endif
                    </p>
                    
                    <div class="flex justify-between items-center mt-4">
                      <p class="font-sans hover:text-blue-600">瞭解更多</p>
                      <button {{ $service->disabled === 1 ? "disabled" : "" }} class="bg-green-600 hover:bg-green-700 px-4 py-2 text-sm text-white rounded-sm transition-all block active:bg-green-800 disabled:cursor-not-allowed disabled:bg-gray-300 disabled:text-gray-500">
                        預定
                      </button>
                    </div>
                  </div>
                </div>
  
              @endforeach
                
            @else

              <p class="font-bold font-sans text-2xl text-gray-500">沒有該服務哦 OwO</p>
                
            @endif

          {{-- <div class="service-box">
            <img src="https://img.freepik.com/free-vector/organic-flat-about-me-landing-page_23-2148881669.jpg" alt="" class="w-full h-full object-cover max-h-[180px]">
            <div class="p-4 flex flex-col">
              <h5 class="text-lg">一頁式網站</h5>
              
              <p class="text-sm text-gray-600 leading-4">適合製作簡單的個人網站或履歷式網站</p>
              
              <button class="bg-green-600 hover:bg-green-700 px-4 py-2 text-sm text-white rounded-sm transition-all mt-4 self-end block">
                預定
              </button>
            </div>
          </div>

          <div class="service-box">
            <img src="https://img.freepik.com/free-vector/organic-flat-about-me-landing-page_23-2148881669.jpg" alt="" class="w-full h-full object-cover max-h-[180px]">
            <div class="p-4 flex flex-col">
              <h5 class="text-lg">多頁網站</h5>
              <p class="text-sm text-gray-600 leading-4">適合較複雜的個人網站</p>
              <button class="bg-green-600 hover:bg-green-700 px-4 py-2 text-sm text-white rounded-sm transition-all mt-4 self-end block">
                預定
              </button>
            </div>
          </div>

          <div class="service-box">
            <img src="https://img.freepik.com/free-vector/organic-flat-about-me-landing-page_23-2148881669.jpg" alt="" class="w-full h-full object-cover max-h-[180px]">
            <div class="p-4 flex flex-col">
              <h5 class="text-lg">商務網站</h5>
              <p class="text-sm text-gray-600 leading-4">想不到要寫什麽 QQ</p>
              <button class="bg-green-600 hover:bg-green-700 px-4 py-2 text-sm text-white rounded-sm transition-all mt-4 self-end block">
                預定
              </button>
            </div>
          </div> --}}

        </section>

      </div>

    </div>

  </section>

@endsection