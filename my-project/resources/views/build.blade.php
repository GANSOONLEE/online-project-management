
@extends('layouts.app')

@section('title', '還未開發的島嶼')

@section('content')

  <div class="w-screen flex flex-col justify-center items-center mt-4">

    <div class="w-3/4">
      <img class="w-full h-full object-cover max-h-[70vh]" src="https://img.freepik.com/free-vector/hand-drawn-404-error-template_23-2147751077.jpg?t=st=1716281952~exp=1716282552~hmac=49839fa93338263fd348b600eb7ac39c205695b51eb2418a200cc2165fa98afe" alt="">
    </div>

    <h1 class="mt-4 text-3xl font-bold">這裏還未開發哦 ~</h1>
  
  </div>

@endsection