
@extends('layouts.app')

@section('title', '服務管理')

@section('content')

  <div class="px-6 py-4 max-w-full">
  
    <h1 class="text-2xl font-bold">服務管理<i class="ml-2 fa fa-cog"></i></h1>

    <div class="mx-auto max-w-screen-2xl">
        <div class="relative overflow-hidden sm:rounded-lg">
            <div class="flex flex-col py-3 space-y-3 lg:flex-row lg:items-center lg:justify-between lg:space-y-0 lg:space-x-4">
                <div class="flex items-center flex-1 space-x-4">
                    <h5>
                        <span class="text-gray-500">服務數量：</span>
                        <span class="">{{ count($services->all()) }}</span>
                    </h5>
                </div>
                <div class="flex flex-col flex-shrink-0 space-y-3 md:flex-row md:items-center lg:justify-end md:space-y-0 md:space-x-3">
                    <button id="create" type="button" class="flex items-center justify-center flex-shrink-0 px-3 py-2 text-sm font-medium text-gray-100 bg-green-600 border border-gray-200 rounded-lg hover:bg-green-700 hover:text-primary-700">
                        <i class="mr-2 fa fa-plus"></i>
                        添加新服務
                    </button>
                </div>
            </div>
            <div class="overflow-x-auto">
                <table class="w-full text-sm text-left text-gray-500 ">
                    <thead class="text-xs text-gray-700 uppercase bg-gray-50">
                        <tr>
                            <th scope="col" class="p-4">
                                <div class="flex items-center">
                                    <input id="checkbox-all" type="checkbox" class="w-4 h-4 bg-gray-100 border-gray-300 rounded text-primary-600 focus:ring-primary-500 ">
                                    <label for="checkbox-all" class="sr-only">checkbox</label>
                                </div>
                            </th>
                            <th scope="col" class="px-4 py-3">名稱</th>
                            <th scope="col" class="px-4 py-3">描述</th>
                            <th scope="col" class="px-4 py-3">類型</th>
                            <th scope="col" class="px-4 py-3">狀態</th>
                            <th scope="col" class="px-4 py-3"></th>
                        </tr>
                    </thead>
                    <tbody>

                      @foreach($services as $service)
                        <tr id="row" class="cursor-pointer border-b bg-gray-100 hover:bg-gray-200 ">
                            <td class="w-4 px-4 py-3">
                                <div class="flex items-center">
                                    <input id="checkbox-table-search-1" type="checkbox" onclick="event.stopPropagation()" class="w-4 h-4 bg-gray-100 border-gray-300 rounded text-primary-600 focus:ring-primary-500">
                                    <label for="checkbox-table-search-1" class="sr-only">checkbox</label>
                                </div>
                            </td>
                            <th scope="row" class="flex items-center px-4 py-2 font-medium text-gray-900 whitespace-nowrap ">
                                {{ $service->name }}
                            </th>
                            <td class="px-4 py-2">
                                <span class="bg-primary-100 text-primary-800 text-xs font-medium px-2 py-0.5 rounded">{{ $service->description }}</span>
                            </td>
                            <td class="px-4 py-2 font-medium text-gray-900 whitespace-nowrap ">
                              {{ $service->type }}
                            </td>
                            <td class="px-4 py-2 font-medium text-gray-900 whitespace-nowrap">
                              <div class="flex items-center">
                                @if ($service->disabled === 1)
                                  <div class="inline-block w-4 h-4 mr-2 bg-red-700 rounded-full"></div>
                                  禁用
                                @else
                                  <div class="inline-block w-4 h-4 mr-2 bg-green-700 rounded-full"></div>
                                  啓用
                                @endif
                              </div>
                            </td>
                            <td>
                              <p data-id="{{ $service->id }}" id="edit" class="cursor-pointer hover:text-blue-600">編輯</p>
                            </td>
                        </tr>
                      @endforeach
                      </tbody>
                    </table>
                </div>
                <nav class="flex flex-col items-start justify-between p-4 space-y-3 md:flex-row md:items-center md:space-y-0" aria-label="Table navigation">
                    <span class="text-sm font-normal text-gray-500 ">
                        第
                        <span class="font-semibold text-gray-900 ">{{ $services->firstItem() . ' - ' . $services->lastItem()}}</span>
                        筆資料，總共
                        <span class="font-semibold text-gray-900 ">{{ $services->total() }}</span>
                        筆
                    </span>
                    <ul class="inline-flex items-stretch -space-x-px">
                        <li>
                            <a href="{{ $services->previousPageUrl() }}" class="flex items-center justify-center h-full py-1.5 px-3 ml-0 text-gray-500 bg-white rounded-l-lg border border-gray-300 hover:bg-gray-100 hover:text-gray-700 ">
                                <span class="sr-only">Previous</span>
                                <svg class="w-5 h-5" aria-hidden="true" fill="currentColor" viewbox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clip-rule="evenodd" />
                                </svg>
                            </a>
                        </li>
                        @foreach ($services->links()->elements[0] as $index => $link)
                            <li>
                                <a href="{{ url($link) }}" class="flex items-center justify-center px-3 py-2 text-sm leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 ">
                                    {{ $index }}
                                </a>
                            </li>
                        @endforeach
                        <li>
                            <a href="{{ $services->nextPageUrl() }}" class="flex items-center justify-center h-full py-1.5 px-3 leading-tight text-gray-500 bg-white rounded-r-lg border border-gray-300 hover:bg-gray-100 hover:text-gray-700 ">
                                <span class="sr-only">Next</span>
                                <svg class="w-5 h-5" aria-hidden="true" fill="currentColor" viewbox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                                </svg>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
  </div>

  <div id="form" class="hidden justify-center items-center top-0 fixed w-screen h-screen bg-black bg-opacity-80 z-[100]">

    <section class="relative p-6 rounded-md shadow bg-white min-w-[420px]">

        <i id="close" class="cursor-pointer fa fa-close text-xl absolute right-4 top-4"></i>

        <form action="{{ route("backend.admin.service.patch", ["id" => ':id']) }}" method="post">
            @csrf
            @method('patch')

            <h3 class="mt-5 text-2xl font-bold text-center">修改服務</h3>

            <div class="flex gap-x-8 mb-4">
                <div class="w-2/3">
                    <label class="block text-gray-700 text-sm font-bold mb-2" for="name">
                    名稱
                    </label>
                    <input name="name" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="name" type="text" placeholder="服務名稱">
                </div>

                <div class="w-1/3">
                    <label class="block text-gray-700 text-sm font-bold mb-2" for="price">
                    價格
                    </label>
                    <div class="relative rounded overflow-hidden">
                        <div class="absolute flex justify-center items-center h-full aspect-square bg-gray-300 text-gray-600 border-transparent">
                            NT
                        </div>
                        <input name="price" class="font-sans ps-12 shadow appearance-none border w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline " id="price" min="0" max="10000" type="number" placeholder="價格">
                    </div>
                </div>

            </div>

            <div class="mb-6">
                <label class="block text-gray-700 text-sm font-bold mb-2" for="description">
                  描述
                </label>
                <textarea name="description" rows="5" class="resize-none shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="description" type="text" placeholder="描述"></textarea>
            </div>

            <div class="mb-6">
                <label class="block text-gray-700 text-sm font-bold mb-2" for="url">
                  圖片網址
                </label>
                <div class="relative rounded overflow-hidden">
                    <div class="absolute flex justify-center items-center h-full px-2 bg-gray-300 text-gray-600 border-transparent">
                        https://
                    </div>
                    <input id="image-url" name="url" class="font-sans ps-[4.5rem] shadow appearance-none border w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" type="string" placeholder="圖片網址">
                </div>
            </div>

            <div class="mb-6">
                <label class="block text-gray-700 text-sm font-bold mb-2" for="price">
                  類型
                </label>
                <select name="type" id="type">
                    <option value="website">網站開發</option>
                    <option value="merchandise">周邊商品</option>
                    <option value="other">其他</option>
                </select>
            </div>
            
            <div class="mb-6">
                <label class="block text-gray-700 text-sm font-bold mb-2" for="price">
                  狀態
                </label>
                <button type="button" id="disabled" class="mr-4 rounded px-3 py-1 text-white"></button>
                <button type="button" id="delete" class="rounded px-3 py-1 text-white bg-red-600">刪除</button>
            </div>

            <div class="flex items-center justify-between">
                <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
                  更新
                </button>
            </div>
    
        </form>

    </section>

  </div>

  <div id="create-form" class="hidden justify-center items-center top-0 fixed w-screen h-screen bg-black bg-opacity-80 z-[100]">

    <section class="relative p-6 rounded-md shadow bg-white min-w-[420px]">

        <i id="create-close" class="cursor-pointer fa fa-close text-xl absolute right-4 top-4"></i>

        <form action="{{ route("backend.admin.service.post") }}" method="post">
            @csrf

            <h3 class="mt-5 text-2xl font-bold text-center">增加服務</h3>

            <div class="flex gap-x-8 mb-4">
                <div class="w-2/3">
                    <label class="block text-gray-700 text-sm font-bold mb-2" for="name">
                    名稱
                    </label>
                    <input name="name" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="name" type="text" placeholder="服務名稱">
                </div>

                <div class="w-1/3">
                    <label class="block text-gray-700 text-sm font-bold mb-2" for="price">
                    價格
                    </label>
                    <div class="relative rounded overflow-hidden">
                        <div class="absolute flex justify-center items-center h-full aspect-square bg-gray-300 text-gray-600 border-transparent">
                            NT
                        </div>
                        <input name="price" class="font-sans ps-12 shadow appearance-none border w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline " id="price" min="0" max="10000" type="number" placeholder="價格">
                    </div>
                </div>

            </div>

            <div class="mb-6">
                <label class="block text-gray-700 text-sm font-bold mb-2" for="description">
                  描述
                </label>
                <textarea name="description" rows="5" class="resize-none shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="description" type="text" placeholder="描述"></textarea>
            </div>

            <div class="mb-6">
                <label class="block text-gray-700 text-sm font-bold mb-2" for="url">
                  圖片網址
                </label>
                <div class="relative rounded overflow-hidden">
                    <div class="absolute flex justify-center items-center h-full px-2 bg-gray-300 text-gray-600 border-transparent">
                        https://
                    </div>
                    <input id="image-url" name="url" class="font-sans ps-[4.5rem] shadow appearance-none border w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" type="string" placeholder="圖片網址">
                </div>
            </div>

            <div class="mb-6">
                <label class="block text-gray-700 text-sm font-bold mb-2" for="price">
                  類型
                </label>
                <select name="type" id="type">
                    <option value="website">網站開發</option>
                    <option value="merchandise">周邊商品</option>
                    <option value="other">其他</option>
                </select>
            </div>

            <div class="flex items-center justify-between">
                <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
                  創建
                </button>
            </div>
    
        </form>

    </section>

  </div>
  
  <script>
  
    const tableRows = document.querySelectorAll('tr#row');
    const form = document.querySelector('#form');
    tableRows.forEach(row => {

        row.addEventListener('click', function () {
            const input = row.querySelector('input[type="checkbox"]');
            input.checked = !input.checked;
        });

        row.querySelector('#edit').addEventListener('click', function () {
            // send request get service data
            getServicedata(row.querySelector("#edit").getAttribute("data-id"));
            
            form.classList.remove("hidden");
            form.classList.add("flex");
        });

    });

    const closeButton = document.querySelector("#close");
    closeButton.addEventListener("click", function () {
        form.classList.add("hidden");
        form.classList.remove("flex");

        form.querySelector("form").setAttribute("action", window.location.origin + "/admin/service/patch/:id")
    });

    async function getServicedata(id)
    {
        let data = "";
        await axios.get(`/api/v1/service/${id}`)
            .then((res) => {
                data = res.data.data;
            });

        let action = form.querySelector("form").getAttribute("action").replace(":id", data.id);
        form.querySelector("form").setAttribute("action", action)
        form.querySelector("#name").value = data.name;
        form.querySelector("#description").value = data.description;
        form.querySelector("#price").value = data.price;
        form.querySelector("#type").value = data.type;
        form.querySelector("#image-url").value = data.url;

        const formButton = form.querySelector("#disabled");
        formButton.setAttribute('data-service-id', data.id)

        if (data.disabled == 1) {
            formButton.classList.remove("bg-yellow-600");
            formButton.classList.add("bg-green-600");
            formButton.innerText = "啓用";
            formButton.setAttribute('data-status-id', 0)
        } else {
            formButton.classList.remove("bg-green-600");
            formButton.classList.add("bg-yellow-600");
            formButton.innerText = "禁用";
            formButton.setAttribute('data-status-id', 1)
        };

    }

    const createForm = document.querySelector('#create-form');
    const createButton = document.querySelector('#create');
    const createCloseButton = document.querySelector('#create-close');

    createButton.addEventListener('click', function () {
        createForm.classList.remove("hidden");
        createForm.classList.add("flex");
    });

    createCloseButton.addEventListener('click', function() {
        createForm.classList.remove("flex");
        createForm.classList.add("hidden");
    });

    const disabledButton = form.querySelector("#disabled");
    const deleteButton = form.querySelector("#delete");

    disabledButton.addEventListener('click', async function () {

        const statusId = disabledButton.getAttribute('data-status-id');
        const id = disabledButton.getAttribute('data-service-id');

        await axios.post(`${window.location}/patch/${id}`, {
            "_method": "patch",
            disabled: statusId
        })

        form.classList.add("hidden");
        form.classList.remove("flex");

        window.location.reload();

    });

    deleteButton.addEventListener('click', async function () {

        const id = disabledButton.getAttribute('data-service-id');

        await axios.post(`${window.location}/delete/${id}`, {
            "_method": "delete",
        })

        form.classList.add("hidden");
        form.classList.remove("flex");

        window.location.reload();

        });
  
  </script>

@endsection