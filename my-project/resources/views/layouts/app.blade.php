<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }} | @yield('title')</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.20.0/axios.min.js"></script>
    @vite(['resources/css/app.css', 'resources/js/bootstrap.js'])
    
</head>
<body>
    <div id="app" class="w-screen min-h-screen bg-gray-100">

        <nav class="flex justify-between w-screen px-8 py-3 bg-primary text-white sticky top-0 z-50">

            <button class="fa fa-bars absolute top-4 left-4 hidden"></button>

            <h3 class="font-bold">綫上服務</h3>

            <ul class="menu gap-x-8">

                <button class="fa fa-close absolute top-4 left-4 hidden"></button>

                <li><a href="{{ route('build') }}">首頁</a></li>
                <li><a href="{{ route('build') }}">關於</a></li>
                <li><a href="{{ route('service') }}">服務項目</a></li>
                <li><a href="{{ route('build') }}">聯係我們</a></li>

                @auth
                    <li><a href="{{ route('backend.admin.service.create') }}">服務管理</a></li>
                    <li><a href="{{ route('build') }}">使用者管理</a></li>
                @endauth

            </ul>

            <ul class="menu gap-x-8">

                @guest
                    <li><a href="{{ route('login') }}">登入</a></li>
                @endguest

                @auth
                    <li><a href="{{ route('logout') }}">登出</a></li>
                @endauth
                {{-- <li><a href="{{ route('build') }}">注冊</a></li> --}}

            </ul>

        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
