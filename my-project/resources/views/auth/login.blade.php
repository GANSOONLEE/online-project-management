
@extends('layouts.app')

@section('title', '登入')

@section('content')

  <div class="flex justify-center items-center w-screen h-screen">

    <form action="{{ route("auth.login.post") }}" method="post" class="flex flex-col shadow-md rounded-md p-8 w-3/4 bg-white">
      @csrf

      <h1 class="text-2xl">登入</h1>

      <div class="flex flex-col gap-y-1 mt-3">
        <label for="email">電子郵件</label>
        <div class="relative ">
          <i class="fa fa-user absolute pl-4 top-1/2 -translate-y-1/2"></i>
          <input required class="w-full ps-10 px-3 py-2 bg-blue-50" type="email" name="email" id="email" placeholder="電子郵件">
        </div>
      </div>

      <div class="flex flex-col gap-y-1 mt-3">
        <label for="password">密碼</label>
        <div class="relative">
          <i class="fa fa-lock absolute pl-4 top-1/2 -translate-y-1/2"></i>
          <input required class="w-full ps-10 px-3 py-2 bg-blue-50" type="password" name="password" id="password" placeholder="密碼">
        </div>
      </div>

      <button class="mt-8 self-end px-3 py-1 rounded-sm text-white bg-green-600 hover:bg-green-700 active:bg-green-800">
        確認
      </button>

    </form>

  </div>

@endsection