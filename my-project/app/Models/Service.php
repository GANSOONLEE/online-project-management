<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

    protected $table = "services";

    protected $fillable = [
        'name',
        'description',
        'disabled',
        'price',
        'url',
        'type'
    ];

    public static function scopeType(string $type = "all")
    {
        if ($type == "all") {
            return self::all();
        } else {
            return self::where("type", $type)->get();
        }
    }
    
}
