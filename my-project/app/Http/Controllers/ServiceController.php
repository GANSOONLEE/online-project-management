<?php

namespace App\Http\Controllers;

use App\Models\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request, $type)
    {

        if ($type == "all") {
            $services = Service::orderBy("type", "desc")->get();
        } else {
            $services = Service::where("type", $type)
                ->get();
        };

        return view('service', compact('services'));
    }

    public function search(Request $request)
    {

        $query = $request->all()['query'];

        if ($query == '') {
            $services = Service::orderBy("type", "desc")->get();
        } else {
            $services = Service::orWhere('type', 'LIKE', '%'.$query.'%')
                ->orWhere('name', 'LIKE', '%'.$query.'%')
                ->orWhere('description', 'LIKE', '%'.$query.'%')
                ->distinct()
                ->get();
        }

        return view('service', compact('services', 'query'));
    }
}
