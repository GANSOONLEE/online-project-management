<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    public function valid(Request $request)
    {
        $result = $request->validate([
            "email" => "required",
            "password" => "required"
        ]);

        if (Auth::attempt($result)) {
            $request->session()->regenerate();
            return redirect()->route("backend.admin.dashboard");
        };

        return redirect()->back();
    }
}
