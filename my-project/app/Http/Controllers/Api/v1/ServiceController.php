<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\v1\ServiceResource;
use App\Http\Resources\v1\ServiceResourceCollection;
use App\Models\Service;

class ServiceController extends Controller
{

    public function index(): ServiceResourceCollection
    {
        return new ServiceResourceCollection(Service::paginate());
    }

    public function show(Service $service)
    {
        return new ServiceResource($service);
    }
}
