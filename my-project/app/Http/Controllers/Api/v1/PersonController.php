<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\v1\PersonResource;
use App\Http\Resources\v1\PersonResourceCollection;
use App\Models\Person;

class PersonController extends Controller
{
    public function show(Person $person)
    {
        return new PersonResource($person);
    }

    public function index(): PersonResourceCollection
    {
        return new PersonResourceCollection(Person::orderBy("type")->paginate());
    }

    public function store(Request $request)
    {
        $request->validate([
            'last_name' => 'required',
            'password'  => 'required',
            'email'     => 'required', 
        ]);

        $person = Person::create($request->all());
        return new PersonResource($person);
    }

    public function update(Person $person, Request $request): PersonResource
    {
        $person->update($request->all());
        return new PersonResource($person);
    }

    public function destroy(Person $person)
    {
        $person->delete();
        return response()->json();
    }
}
