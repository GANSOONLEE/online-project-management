<?php

namespace App\Http\Controllers\Api\v2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\v2\PersonResource;
use App\Http\Resources\v2\PersonResourceCollection;
use App\Models\Person;

class PersonController extends Controller
{
    public function index(): PersonResourceCollection
    {
        return new PersonResourceCollection(Person::paginate());
    }

    public function show(Person $person)
    {
        return new PersonResource($person);
    }
    
}
