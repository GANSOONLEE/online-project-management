<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ServiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray($request): array
    {
        return [
          'id' => $this->id,
          'name' => $this->name,
          'description' => $this->description,
          'disabled' => $this->disabled,
          'price' => $this->price,
          'type' => $this->type,
          'url' => $this->url,
        ];
    }
}
