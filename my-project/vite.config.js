import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import vue from '@vitejs/plugin-vue';
import basicSsl from '@vitejs/plugin-basic-ssl'

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/css/app.css',
                'resources/js/bootstrap.js',
            ],
            refresh: true,
        }),
        vue({
            template: {
                transformAssetUrls: {
                    base: null,
                    includeAbsolute: false,
                },
            },
        }),
    ],
    resolve: {
        alias: {
            vue: 'vue/dist/vue.esm-bundler.js',
        },
    },
    server: {
        hmr: {
            clientPort: 80,
            host: "https://5173-gansoonlee-onlineprojec-4bnbd6ifbsg.ws-us114.gitpod.io",
        },
        origin: "https://5173-gansoonlee-onlineprojec-4bnbd6ifbsg.ws-us114.gitpod.io",
        
    }
});
