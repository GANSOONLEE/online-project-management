<?php

use App\Http\Middleware\CorsMiddleware;
use Illuminate\Foundation\Application;
use Illuminate\Foundation\Configuration\Exceptions;
use Illuminate\Foundation\Configuration\Middleware;

return Application::configure(basePath: dirname(__DIR__))
    ->withRouting(
        web: __DIR__.'/../routes/web.php',
        api: __DIR__.'/../routes/api.php',
        commands: __DIR__.'/../routes/console.php',
        health: '/up',
        then: function () {

            /** 驗證相關 */
            Route::middleware('web')
                ->prefix('auth')
                ->group(base_path('routes/auth.php'));

            /** 使用者後端 */
            Route::middleware(['web', 'auth'])
                ->prefix('user')
                ->name('backend.user.')
                ->group(base_path('routes/user.php'));

            /** 管理者後端 */
            Route::middleware(['web', 'auth'])
                ->prefix('admin')
                ->name('backend.admin.')
                ->group(base_path('routes/admin.php'));
        }
    )
    ->withMiddleware(function (Middleware $middleware) {
        $middleware->append(CorsMiddleware::class);
    })
    ->withExceptions(function (Exceptions $exceptions) {
        //
    })->create();
