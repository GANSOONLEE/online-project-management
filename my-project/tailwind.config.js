/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./resources/**/*.blade.php",
    "./resources/**/*.vue",
  ],
  theme: {
    extend: {
      colors: {
        'primary': '#23232b',
      },
      fontFamily: {
        notoSerifTC: [
          "Noto Serif TC"
        ],
        notoSansTc: [
          "Noto Sans TC"
        ]
      }
    },
  },
  plugins: [],
}

